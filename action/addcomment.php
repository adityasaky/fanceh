<?php
session_start();
require '../includes/head.php';

if ($_SESSION['login_check']) {

  $comment = $_POST['comment'];
  $comment = $conn->real_escape_string($comment);

  $task_id = $_POST['task_id'];
  $task_id = $conn->real_escape_string($task_id);

  $user_id = $_SESSION['user_id'];
  $user_id = $conn->real_escape_string($user_id);

  $date = date('Y-m-d H:i:s');

  $sql = "INSERT INTO comments (Comment, Task_ID, User_ID, Date) values ('$comment', '$task_id', '$user_id', '$date')";
  if (mysqli_query($conn, $sql)) {
    header("Location: $baseurl/view/task.php?id=$task_id");
  } else {
    echo mysqli_error($conn);
  }
} else {
  $_SESSION['login_check'] = 0;
  header("Location: $baseurl");
}


?>
