<?php
require '../includes/head.php';

if ($_SESSION[login_check] && $_SESSION['user_privilege']) {
  $name = $_POST['name'];
  $name = $conn->real_escape_string($name);
  $password = $_POST['password'];
  $password = $conn->real_escape_string($password);
  $cpassword = $_POST['cpassword'];
  $cpassword = $conn->real_escape_string($cpassword);
  $email = $_POST['email'];
  $email = $conn->real_escape_string($email);
  $privilege = $_POST['privilege'];
  $privilege = $conn->real_escape_string($privilege);

  if (!strcmp($password, $cpassword)) {
    $password = password_hash($password, PASSWORD_BCRYPT);
    $sql = "INSERT INTO users (Name, Email, Password, Privilege) values ('$name', '$email', '$password', '$privilege')";
    if (mysqli_query($conn, $sql)) {
      header("Location: $baseurl/view/adduser.php");
    }
    else {
      echo mysqli_error($conn);
    }
  }
  else {
    echo 'Passwords do not match.';
  }
}
?>
