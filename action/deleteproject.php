<?php
  session_start();
  require '../includes/head.php';

  if ($_SESSION['login_check']) {

    $id = $_GET['id'];

    $sql = "SELECT * FROM projects WHERE ID='$id'";
    $project = mysqli_query($conn, $sql);
    if (!mysqli_num_rows($project)) {
      echo 'Project does not exist';
    }
    else {
      $sql = "DELETE FROM projects WHERE ID='$id'";
      if (mysqli_query($conn, $sql)) {
        header("Location: $baseurl/view/allprojects.php");
      }
      else {
        echo mysqli_error($conn);
      }
    }
  }
  else {
    $_SESSION['login_check'] = 0;
    header("Location: $baseurl");
  }

?>
