<?php
  require '../includes/head.php';

  $email = $_POST['email'];
  $email = $conn->real_escape_string($email);

  $password = $_POST['password'];
  $password = $conn->real_escape_string($password);

  $sql = "SELECT * FROM users WHERE Email='$email'";

  $user = mysqli_query($conn, $sql);
  if (mysqli_num_rows($user)) {
    $user = mysqli_fetch_object($user);
    if (password_verify($password, $user->Password)) {
      $_SESSION['login_check'] = 1;
      $_SESSION['user_name'] = $user->Name;
      $_SESSION['user_email'] = $email;
      $_SESSION['user_privilege'] = $user->Privilege;
      $_SESSION['user_id'] = $user->ID;
      header("Location: $baseurl");
    }
    else {
      $_SESSION['login_check'] = 0;
      header("Location: $baseurl/view/signin.php");
    }
  }
  else {
    $_SESSION['login_check'] = 0;
    header("Location: $baseurl/view/signin.php");
  }
?>
