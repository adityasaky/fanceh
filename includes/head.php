<?php
session_start();
$baseurl = 'http://127.0.0.1/fanceh';
$basetitle = 'Fanceh';
$basesubtitle = 'The KISS Task Management System';
date_default_timezone_set('Asia/Calcutta');
$host = 'localhost';
$user = 'root';
$password = '';
$database = 'fanceh';
$conn = mysqli_connect("$host", "$user", "$password", "$database");
?><!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?=$basetitle ?> - <?=$basesubtitle ?></title>
  <link rel="stylesheet" href="<?=$baseurl ?>/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=$baseurl ?>/css/style.css">
</head>
