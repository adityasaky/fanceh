
<!-- Bootstrap Core JavaScript -->
<script src="<?=$baseurl ?>/js/jquery.min.js"></script>
<script src="<?=$baseurl ?>/js/bootstrap.min.js"></script>

<script type="text/javascript">
$(document).click(function (event) {
    var clickover = $(event.target);
    var $navbar = $(".navbar-collapse");
    var _opened = $navbar.hasClass("in");
    if (_opened === true && !clickover.hasClass("navbar-toggle")) {
        $navbar.collapse('hide');
    }
});
</script>
