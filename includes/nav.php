<!-- Nav bar -->
<nav class="navbar navbar-inverse navbar-static-top navbar-style">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=$baseurl ?>"><?=$basetitle ?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?=$baseurl ?>/view/allprojects.php">Projects</a></li>
        <?php
        if ($_SESSION['user_privilege'] == 1) {
          ?>
          <li><a href="<?=$baseurl ?>/view/adduser.php">Add User</a></li>
          <?php
        }
        ?>
        <li><a href="<?=$baseurl ?>/action/logout.php">Logout</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
