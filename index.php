<?php
require 'includes/head.php';
if ($_SESSION['login_check'] == 1) {
  $user_name = $_SESSION['user_name'];
  $user_email = $_SESSION['user_email'];
  $user_privilege = $_SESSION['user_privilege'];
  $user_id = $_SESSION['user_id'];
  ?>
  <body>
    <?php
    require 'includes/nav.php';
    ?>
    <div class="container">
      <div class="profile">
        <h2>
          Hi<?php
          if ($user_name != '') {
            echo ', ' . $user_name;
          }
          ?>!
        </h2>
        <div class="badge"><?php
        if ($user_privilege == 1) {
          echo 'Admin';
        }
        else {
          echo 'User';
        }
        ?></div>
    </div>
    <div class="profile">
      <form class="form" name="profile" method="post" action="<?=$baseurl ?>/action/profile.php">
        <div class="col-md-6">
          <div class="form-group">
            <input type="email" class="form-control" name="email" value="<?=$user_email ?>" readonly>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="name" value="<?=$user_name ?>" placeholder="Name" required>
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="cpassword" placeholder="Confirm Password">
          </div>
          <div class="form-group">
            <input type="submit" class="btn btn-success" name="submit" value="Update Profile">
          </div>
        </div>
      </div>
    </div>
    <?php
    require 'includes/js.php';
    ?>
  </body>
  <?php
  require 'includes/footer.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
