<?php
require '../includes/head.php';
if ($_SESSION['login_check'] == 1) {
  $user_name = $_SESSION['user_name'];
  $user_email = $_SESSION['user_email'];
  $user_privilege = $_SESSION['user_privilege'];
  $user_id = $_SESSION['user_id'];
  ?>
  <body>
    <?php
    require '../includes/nav.php';
    ?>
    <div class="container">
      <div class="profile">
        <h2>
          Add Project
        </h2>
      </div>
      <div class="profile">
        <form class="form" name="addproject" method="post" action="<?=$baseurl ?>/action/addproject.php">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" class="form-control" name="name" placeholder="Name" required>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="description" placeholder="Description" rows=5 required></textarea>
            </div>
            <div class="form-group">
              <input type="date" name="deadline" class="form-control" required>
            </div>
            <div class="form-group">
              <input type="submit" class="btn btn-success" name="submit" value="Add Project">
            </div>
          </div>
        </form>
      </div>
    </div>
    <?php
    require '../includes/js.php';
    ?>
  </body>
  <?php
  require '../includes/footer.php';
} else {
  header("Location: $baseurl/view/signin.php");
}
?>
