<?php
require '../includes/head.php';
if ($_SESSION['login_check'] == 1) {
  $project_id = $_GET['project_id'];
  $sql = "SELECT * FROM projects WHERE ID='$project_id'";
  $project = mysqli_query($conn, $sql);
  if (mysqli_num_rows($project)) {
    $project = mysqli_fetch_object($project);
    ?>
    <body>
      <?php
      require '../includes/nav.php';
      ?>
      <div class="container">
        <div class="profile">
          <h2>
            Add Task to "<?=$project->Name ?>"
          </h2>
        </div>
        <div class="profile">
          <form class="form" name="addtask" method="post" action="<?=$baseurl ?>/action/addtask.php">
            <div class="col-md-6">
              <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Name" required>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="description" placeholder="Description" rows=5 required></textarea>
              </div>
              <div class="form-group">
                <select class="form-control" name="priority">
                  <option value="3">Low</option>
                  <option value="2">Medium</option>
                  <option value="1">High</option>
                </select>
              </div>
              <input type="hidden" value="<?=$project_id ?>" name="project_id">
              <div class="form-group">
                <input type="submit" class="btn btn-success" name="submit" value="Add Task to <?=$project->Name ?>">
              </div>
            </div>
          </form>
        </div>
      </div>
      <?php
      require '../includes/js.php';
      ?>
    </body>
    <?php
    require '../includes/footer.php';
  }  else {
    echo 'Project not found.';
  }
} else {
  header("Location: $baseurl/view/signin.php");
}
?>
