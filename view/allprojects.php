<?php
require '../includes/head.php';
if ($_SESSION['login_check'] == 1) {
  $user_name = $_SESSION['user_name'];
  $user_email = $_SESSION['user_email'];
  $user_privilege = $_SESSION['user_privilege'];
  $user_id = $_SESSION['user_id'];
    ?>
    <body>
      <?php
      require '../includes/nav.php';
      ?>
      <div class="container">
        <div class="profile">
          <h2>
            All Projects
          </h2>
          <a class="btn btn-warning" href="<?=$baseurl ?>/view/addproject.php">Add Project</a>
        </div>
        <div class="profile">
          <?php
          $sql = "SELECT * FROM projects";
          $projects = mysqli_query($conn, $sql);
          if (!mysqli_num_rows($projects)) {
            echo 'No projects found.';
          } else {
            ?>
            <table class="table">
              <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Deadline</th>
                <th>Status</th>
                <th>Delete</th>
              </thead>
              <tbody>
                <?php
                while ($project = mysqli_fetch_object($projects)) {
                  ?>
                  <tr>
                    <td><?=$project->ID ?></td>
                    <td><a href="<?=$baseurl ?>/view/project.php?id=<?=$project->ID ?>"><?=$project->Name ?></a></td>
                    <td><?=$project->Deadline ?></td>
                    <td>
                      <?php
                      if ($project->Status) {
                        ?>Closed (<a href="<?=$baseurl ?>/action/toggleprojectstatus.php?id=<?=$project->ID ?>">Open</a>)
                        <?php
                      } else {
                        ?>Open (<a href="<?=$baseurl ?>/action/toggleprojectstatus.php?id=<?=$project->ID ?>">Close</a>)
                        <?php
                      }
                      ?>
                      <td><a href="<?=$baseurl ?>/action/deleteproject.php?id=<?=$project->ID ?>">Delete</a></td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
              <?php
            }
            ?>
          </div>
        </div>
        <?php
        require '../includes/js.php';
        ?>
      </body>
      <?php
      require '../includes/footer.php';
  } else {
    header("Location: $baseurl/view/signin.php");
  }
  ?>
