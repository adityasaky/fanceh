<?php
require '../includes/head.php';
if ($_SESSION['login_check'] == 1) {
  $id = $_GET['id'];
  $sql = "SELECT * FROM projects WHERE ID='$id'";
  $project = mysqli_query($conn, $sql);
  if (!mysqli_num_rows($project)) {
    echo 'Project not found.';
  } else {
    $project = mysqli_fetch_object($project);
    $sql = "SELECT * FROM tasks WHERE Project_ID='$id'";
    $total_tasks = mysqli_query($conn, $sql);
    $total_tasks = mysqli_num_rows($total_tasks);
    $sql = "SELECT * FROM tasks WHERE Project_ID='$id' AND Status=1";
    $completed_tasks = mysqli_query($conn, $sql);
    $completed_tasks = mysqli_num_rows($completed_tasks);
    $percent = $completed_tasks * 100 / $total_tasks;
    ?>
    <body>
      <?php
      require '../includes/nav.php';
      ?>
      <div class="container">
        <div class="profile">
          <h2>
            View Project
          </h2>
        </div>
        <div class="profile">
          <div class="col-md-6">
            <form class="form" name="project" method="post" action="<?=$baseurl ?>/action/project.php">
              <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="name" value="<?=$project->Name ?>" required>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="description" placeholder="Description" required><?=$project->Description ?></textarea>
              </div>
              <div class="form-group">
                <input type="date" class="form-control" name="deadline" placeholder="Deadline" value="<?=$project->Deadline ?>" required>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-success" name="submit" value="Edit Project">
              </div>
            </form>
          </div>
          <div class="col-md-6">
            <h4>
              Progress
            </h4>
            <div class="progress">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?=$completed_tasks ?>" aria-valuemin="0" aria-valuemax="<?=$total_tasks ?>" style="width: <?=$percent ?>%">
                <?php if ($completed_tasks) { ?><?=$completed_tasks ?> / <?=$total_tasks ?> <?php } ?>
              </div>
            </div>
            <?php
            if ($percent == '100') {
              echo '<h4>All complete!</h4>';
            }
            ?>
          </div>
        </div>
        <div class="profile">
          <div class="col-md-12">
            <a class="btn btn-warning" href="<?=$baseurl ?>/view/addtask.php?project_id=<?=$id ?>">Add Task</a>
            <?php
            $sql = "SELECT * FROM tasks WHERE Project_ID='$id'";
            $tasks = mysqli_query($conn, $sql);
            if (mysqli_num_rows($tasks)) {
              ?>
              <table class="table">
                <thead>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Status</th>
                  <th>Priority</th>
                  <th>Delete</th>
                </thead>
                <tbody>
                  <?php

                  while ($task = mysqli_fetch_object($tasks)) {

                    ?>
                    <tr>
                      <td><?=$task->ID ?></td>
                      <td><a href="<?=$baseurl ?>/view/task.php?id=<?=$task->ID ?>"><?=$task->Name ?></a></td>
                      <td>
                        <?php
                        if ($task->Status) {
                          ?>Closed (<a href="<?=$baseurl ?>/action/toggletaskstatus.php?id=<?=$task->ID ?>">Open</a>)
                          <?php
                        }
                        else {
                          ?>Open (<a href="<?=$baseurl ?>/action/toggletaskstatus.php?id=<?=$task->ID ?>">Close</a>)
                          <?php
                        }
                        ?>
                      </td>
                      <td>
                        <?php
                        switch ($task->Priority) {
                          case 1: echo 'High';
                          break;
                          case 2: echo 'Medium';
                          break;
                          case 3: echo 'Low';
                          break;
                          default: echo 'Error';
                        }
                        ?>
                      </td>
                      <td><a href="<?=$baseurl ?>/action/deletetask.php?id=<?=$task->ID ?>">Delete</td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
                <?php
              } else {
                echo 'No tasks at present.';
              }
              ?>
            </div>
          </div>
        </div>
        <?php
        require '../includes/js.php';
        ?>
      </body>
      <?php
      require '../includes/footer.php';
    }
  } else {
    header("Location: $baseurl/view/signin.php");
  }
  ?>
