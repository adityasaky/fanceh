<?php
require '../includes/head.php';
  ?>
  <body>
    <div class="container">
      <div class="profile">
        <h2>
          Sign In
        </h2>
        <form class="form" method="post" action="../action/signin.php">
          <div class="col-md-6">
          <div class="form-group">
            <input class="form-control" name="email" type="email" placeholder="Email" required autofocus>
          </div>
          <div class="form-group">
            <input class="form-control" name="password" type="password" placeholder="Password" required>
          </div>
          <div class="form-group">
            <input class="btn btn-success" name="submit" type="submit" value="Sign In">
          </div>
        </div>
        </form>
      </div>
    </div>
    <?php
    require 'includes/js.php';
    ?>
  </body>
  <?php
  require 'includes/footer.php';
?>
