<?php
require '../includes/head.php';
if ($_SESSION['login_check'] == 1) {
  $id = $_GET['id'];
  $sql = "SELECT * FROM tasks WHERE ID='$id'";
  $task = mysqli_query($conn, $sql);
  if (!mysqli_num_rows($task)) {
    echo 'Task not found.';
  }
  else {
    $task = mysqli_fetch_object($task);
    ?>
    <body>
      <?php
      require '../includes/nav.php';
      ?>
      <div class="container">
        <div class="profile">
          <h2>
            View Task
          </h2>
        </div>
        <div class="profile">
          <div class="col-md-6">
            <form class="form" name="task" method="post" action="<?=$baseurl ?>/action/task.php">
              <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="name" value="<?=$task->Name ?>" required>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="description" placeholder="Description" required><?=$task->Description ?></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-success" name="submit" value="Edit Task">
              </div>
            </form>
          </div>
          <div class="col-md-6">
            <form method="post" action="../action/addcomment.php" name="addcomment">
              <div class="form-group">
                <textarea class="form-control" name="comment" placeholder="Comment" required></textarea>
              </div>
              <input type="hidden" name="task_id" value="<?=$id ?>">
              <div class="form-group">
                <input type="submit" name="submit" class="btn btn-success" value="Add Comment">
              </div>
            </form>
          </div>
        </div>
        <div class="profile">
          <div class="col-md-12">
            <h2>
              Comments
            </h2>
          </div>
        </div>
        <div class="profile">
          <div class="col-md-12">
            <?php
            $sql = "SELECT * FROM comments WHERE Task_ID='$id' ORDER BY Date";
            $comments = mysqli_query($conn, $sql);
            if (mysqli_num_rows($comments)) {
              while ($comment = mysqli_fetch_object($comments)) {
                $sql = "SELECT * FROM users WHERE ID='$comment->User_ID'";
                $user = mysqli_query($conn, $sql);
                $user = mysqli_fetch_object($user);
                ?>
                <div class="comment">
                  <?=$user->Name ?> says...<br>
                  <?=$comment->Comment ?>
                </div>
                <?php
              }
            } else {
              echo 'No comments found.';
            }
            ?>
          </div>
        </div>
      </div>
      <?php
      require '../includes/js.php';
      ?>
    </body>
    <?php
    require '../includes/footer.php';
  }
} else {
  header("Location: $baseurl/view/signin.php");
}
?>
